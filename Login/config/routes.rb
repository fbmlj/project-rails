Rails.application.routes.draw do
  
  resources :friend_ships
  get 'friendships/create'
  get 'friendships/destroy'
  resources :likes
  resources :comments
  resources :posts
  root 'static_pages#home'
  
  resources :users, except: [:new],path:'usuarios'
  
  get '/cadastro',to: 'users#new'

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  resources :users do
    resources :friend_ships    
    resources :posts do
      resources :comments
      resources :likes
    end
  end
end
