class User < ApplicationRecord
    has_secure_password
    has_many :posts
    has_many :comments
    has_many :likes
    validates :password, presence: true,length: {minimum: 6 }
    has_many :friendships
    has_many :friends, :through => :friendships    
end 