require "application_system_test_case"

class FriendShipsTest < ApplicationSystemTestCase
  setup do
    @friend_ship = friend_ships(:one)
  end

  test "visiting the index" do
    visit friend_ships_url
    assert_selector "h1", text: "Friend Ships"
  end

  test "creating a Friend ship" do
    visit friend_ships_url
    click_on "New Friend Ship"

    fill_in "Friend", with: @friend_ship.friend_id
    fill_in "User", with: @friend_ship.user_id
    click_on "Create Friend ship"

    assert_text "Friend ship was successfully created"
    click_on "Back"
  end

  test "updating a Friend ship" do
    visit friend_ships_url
    click_on "Edit", match: :first

    fill_in "Friend", with: @friend_ship.friend_id
    fill_in "User", with: @friend_ship.user_id
    click_on "Update Friend ship"

    assert_text "Friend ship was successfully updated"
    click_on "Back"
  end

  test "destroying a Friend ship" do
    visit friend_ships_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Friend ship was successfully destroyed"
  end
end
